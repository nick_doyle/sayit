#!/usr/bin/env python
import subprocess
import sys
import re
import random
import time

say_exe_names = ['espeak', 'say']
say_exe_path = None
voices = []

for say_exe_name in say_exe_names:
    cmd = ['which', say_exe_name]
    stdout, stderr = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
    say_exe_path = stdout.strip()
    if say_exe_path:
        print 'using %s' % say_exe_name

        if 'say' == say_exe_name:
            cmd = [say_exe_path, '-v', '?']
            stdout, stderr = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
            for line in stdout.split('\n'):
                try:
                    voices.append(re.match('^(\w+)', line).groups()[0])
                except (IndexError, AttributeError):
                    pass
        elif 'espeak' == say_exe_name:
            cmd = [say_exe_path, '--voices']
            stdout, stderr = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
            for line in stdout.split('\n'):
                try:
                    voicename = re.match('^(\s*\S*\s*){3}(\w+)', line).groups()[1]
                    if 'VoiceName' != voicename:
                        voices.append(voicename)
                except (IndexError, AttributeError):
                    pass
        # We have one say exe, get out of here
        break

if not say_exe_path:
    raise Exception("Couldn't find espeak or say")

#for line in sys.stdin:
for line in open('neuromancer.txt', 'r'):
    line = line.strip()
    print line
    for word in [x for x in line.split() if x]:
        cmd = [say_exe_path, '-v', random.choice(voices), word]
        subprocess.Popen(cmd)
        time.sleep(len(word) * 0.1)

print 'THE END'
